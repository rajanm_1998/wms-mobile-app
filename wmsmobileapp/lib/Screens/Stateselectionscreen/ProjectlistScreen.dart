import 'package:flutter/material.dart';
import 'package:wmsmobileapp/Model/Project/ProjectModel.dart';
import 'package:wmsmobileapp/Operation/ProjectOperation/StateselectionOperation.dart';


class ProjectlistScreen extends StatefulWidget {
  ProjectlistScreen({Key? key}) : super(key: key);
  @override
  State<ProjectlistScreen> createState() => _ProjectlistScreenState();
}

class _ProjectlistScreenState extends State<ProjectlistScreen> {
  void initState() {
    super.initState();
    ConvertProjectDetails();
  }

  int count = 0;
  //Converting data into display format
  List<ProjectModel>? projectlist;
  Set<String>? ProjectName;
  ConvertProjectDetails() async{
    var _projectlist = getProjectAuthority();
    projectlist = await _projectlist;
   ProjectName = Set<String>();
    setState(() {
      projectlist?.forEach((e) => ProjectName?.add(e.projectName!));
    });

    return _projectlist;
  }
  

  //Project data view
  Widget listViewBuilder(BuildContext context, List<ProjectModel> values) {
    
    return ListView.builder(
        itemCount: projectlist?.length,
        itemBuilder: (BuildContext context, int index) {
          return Card(
            child: InkWell(
              // onTap: () async {
              //   await SecureStorage.setProjectId(
              //       values[index].projectId.toString());
              //   print(values[index].projectId);
              //   // Navigator.push(
              //   //     context,
              //   //     MaterialPageRoute(
              //   //         builder: (context) => DynamicDashboardScreen()));
              // },
              child: Container(
                color: const Color.fromRGBO(40, 38, 56, 1),
                width: 18.0,
                child: ListTile(
                  trailing: Icon(Icons.arrow_forward_ios_rounded),
                  title: Text(
                    ProjectName!.elementAt(index),
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 19.0,
                        color: Colors.white),
                  ),
                  // subtitle: Padding(
                  //   padding: const EdgeInsets.all(10.0),
                  //   child: Column(
                  //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  //     crossAxisAlignment: CrossAxisAlignment.stretch,
                  //     children: <Widget>[
                  //       Padding(
                  //         padding: const EdgeInsets.all(2.0),
                  //         child: Text(
                  //           "State:" + values[index].state!,
                  //         ),
                  //       ),
                  //     ],
                  //   ),
                  // ),
                
                ),
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Projects List'),
          backgroundColor: const Color.fromRGBO(40, 38, 56, 1),
        ),
        body: FutureBuilder(
          future: ConvertProjectDetails(),
          builder: (context, value) {
            if (value.hasData) {
              return listViewBuilder(context, projectlist!);
            } else if (value.hasError) {
              return Center(child: Text("No Data Found"));
            } else {
              return Center(child: new CircularProgressIndicator());
            }
          },
        ));
  }
}



