import 'package:flutter/material.dart';
import 'package:wmsmobileapp/Model/Project/ProjectModel.dart';
import 'package:wmsmobileapp/Operation/ProjectOperation/StateselectionOperation.dart';
import 'package:wmsmobileapp/Screens/Stateselectionscreen/ProjectlistScreen.dart';

class StateselectionScreen extends StatefulWidget {
  StateselectionScreen({Key? key}) : super(key: key);
  @override
  State<StateselectionScreen> createState() => _StateselectionScreenState();
}

class _StateselectionScreenState extends State<StateselectionScreen> {
  void initState() {
    super.initState();
    ConvertProjectDetails();
  }

  int count = 0;
  //Converting data into display format
  List<ProjectModel>? stateselection;
  List<ProjectModel>? StateList;
  ConvertProjectDetails() async {
    var _stateselection = await getProjectAuthority();
    var seen = Set<String>();
    StateList = _stateselection.where((e) => seen.add(e.state!)).toList();
    
    
    

    return _stateselection;
  }

  //Project data view
  Widget listViewBuilder(BuildContext context, List<ProjectModel> values) {
    return ListView.builder(
        itemCount: values.length,
        itemBuilder: (BuildContext context, int index) {
          return Card(
            child: InkWell(
              // onTap: () {

              //      Navigator.push(
              //       context,
              //       MaterialPageRoute(
              //           builder: (context) => ProjectlistScreen()));
              // },
              //   await SecureStorage.setProjectId(
              //       values[index].projectId.toString());
              //   print(values[index].projectId);
              //   // Navigator.push(
              //   //     context,
              //   //     MaterialPageRoute(
              //   //         builder: (context) => DynamicDashboardScreen()));
              // },
              child: Container(
                color: const Color.fromRGBO(40, 38, 56, 1),
                width: 18.0,
                child: ListTile(
                  trailing: Icon(Icons.arrow_forward_ios_rounded),
                  title: Text(
                    values[index].state!,
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 19.0,
                        color: Colors.white),
                  ),
                  // subtitle: Padding(
                  //   padding: const EdgeInsets.all(10.0),
                  //   child: Column(
                  //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  //     crossAxisAlignment: CrossAxisAlignment.stretch,
                  //     children: <Widget>[
                  //       Padding(
                  //         padding: const EdgeInsets.all(2.0),
                  //         child: Text(
                  //           "State:" + values[index].state!,
                  //         ),
                  //       ),
                  //     ],
                  //   ),
                  // ),
                ),
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Projects'),
          backgroundColor: const Color.fromRGBO(40, 38, 56, 1),
        ),
        body: FutureBuilder(
          future: ConvertProjectDetails(),
          builder: (context, value) {
            if (value.hasData) {
              return listViewBuilder(context, StateList!);
            } else if (value.hasError) {
              return Center(child: Text("No Data Found"));
            } else {
              return Center(child: new CircularProgressIndicator());
            }
          },
        ));
  }
}
