import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:wmsmobileapp/Model/Project/ProjectModel.dart';
import 'package:wmsmobileapp/Model/CommonModel/Constants.dart';

Future<List<ProjectModel>> getProjectAuthority() async {
  try {
    
  final response = await http
      .get(Uri.parse('http://wmsservices.seprojects.in/api/Project/GetProjectAuthority?userid=30020'));

  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    var json = jsonDecode(response.body);
    if(json['Status'] == WebApiStatusOk)
    {
      List<ProjectModel> project = <ProjectModel>[];
      json['data']['Response'].forEach((e)=> project.add(new ProjectModel.fromJson(e)));

    return project;
    }
    else
    throw Exception('Failed to load API');
  }
   else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load API');
  }
  } 
    catch (e) {
      throw Exception('Failed to load API');
  }
}


// var request = http.Request('GET', Uri.parse('http://wmsservices.seprojects.in/api/Project/GetProjectAuthority?userid=30020'));


// http.StreamedResponse response = await request.send();

// if (response.statusCode == 200) {
//   print(await response.stream.bytesToString());
// }
// else {
//   print(response.reasonPhrase);
// }
