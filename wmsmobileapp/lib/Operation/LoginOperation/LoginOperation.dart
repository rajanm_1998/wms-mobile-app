//System Packages
import 'package:http/http.dart' as http;
import 'package:wmsmobileapp/Model/CommonModel/Constants.dart';
import 'dart:async';
import 'dart:convert';

//File Imports
import "package:wmsmobileapp/Model/LoginModel/LoginModel.dart";




Future<LoginModel?>  fetchLoginDetails(String mobno, String passwd) async{
  try{
    final response = await http.get(Uri.parse(GetHttpRequest(WebApiLoginPrefix, 'Login?MobNo=${mobno}&Password=${passwd}')));
    if(response.statusCode == 200){
      var json = jsonDecode(response.body);
      if(json['Status'] == WebApiStatusOk)
      {
      //Single Model
      LoginModel loginResult = LoginModel.fromJson(json['data']['Response']);
      
      //Collection Model
        /*
        List<LoginModel> loginResult = <LoginModel>[];
        json['Response'].forEach((v) => LoginModel.fromJson(v));
        */
      print(loginResult);
      return loginResult;
      }
      else
        throw Exception("Login Failed");
    }else{
      throw Exception("Login Failed");
    }
  }on Exception catch(_, ex){
    return null;
  }
}

String GetHttpRequest(String ApiPrefix, String CallName)
{
  var url = WebApiUrl + ApiPrefix + CallName;
  return url;
}